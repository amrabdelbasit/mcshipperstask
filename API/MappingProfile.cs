﻿using AutoMapper;
using Domain.DTO.Account;
using Domain.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<User, UserResponseData>();
			CreateMap<UserResponseData, User>();
		}
	}
}
