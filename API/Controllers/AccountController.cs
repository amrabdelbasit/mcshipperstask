﻿using Application.Interfaces;
using Domain.DTO.Account;
using Domain.Models.Account;
using Domain.Models.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IAccount _account;

        public AccountController(IAccount account)
        {
            this._account = account;
        }

               
        [HttpPost("Register")]
        public async Task<IActionResult> Register(RegisterRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ServiceResponse<bool> response = await _account.Register(request);

            if (!response.Success) return BadRequest(response);
            return Ok(response);
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(LoginRequest loginRequest)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ServiceResponse<UserResponseData> response = await _account.Login(loginRequest);

            if (!response.Success) return BadRequest(response);
            return Ok(response);
        }

        [Authorize]
        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword(string newPassword)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var email = User.FindFirstValue(ClaimTypes.NameIdentifier);
            ServiceResponse<bool> response = await _account.ResetPassword(email, newPassword);

            if (!response.Success) return BadRequest(response);
            return Ok(response);
        }                     
    }
}
