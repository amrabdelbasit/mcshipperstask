﻿using Application.Interfaces;
using Domain.Models.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RLLibrary.DTO.Rate;
using RLLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class RateController : BaseController
    {
        private readonly IRate _rate;

        public RateController(IRate rate)
        {
            _rate = rate;
        }

        [Authorize]
        [HttpPost("EstimateRate")]
        public async Task<IActionResult> EstimateRate(RateRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var email = User.FindFirstValue(ClaimTypes.NameIdentifier);
            ServiceResponse<RateResponse> response = await _rate.EstimateRate(email, request);

            if (!response.Success) return BadRequest(response);
            return Ok(response);
        }
    }
}
