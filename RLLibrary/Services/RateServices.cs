﻿using Application.Interfaces;
using Domain.Models.Account;
using Domain.Models.Shared;
using Microsoft.EntityFrameworkCore;
using RLLibrary.DTO.Rate;
using RLLibrary.Interfaces;
using RLLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLLibrary.Services
{
    public class RateServices : IRate
    {
        private readonly IUnitOfWork _unitOfWork;

        public RateServices(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ServiceResponse<bool>> AddItemClasses(string email, List<decimal> classesNumbers)
        {
            var response = new ServiceResponse<bool>();

            try
            {
                var isUserExist = await _unitOfWork
                    .GenericRepository<User>()
                    .Get(x =>
                    x.IsActive &&
                    x.Email == email
                    )
                    .AnyAsync();

                if (isUserExist)
                {
                    response.Message = "This user is not exist!";
                    response.Success = false;
                    return response;
                }

                var classes = new List<Class>();
                foreach (var c in classesNumbers)
                {
                    classes.Add(new Class
                    {
                        ClassNumber = c
                    });
                }                

                _unitOfWork.GenericRepository<Class>().InsertAll(classes);
                _unitOfWork.Save();

                response.Message = "Classes added successfully!";
                response.Success = true;
                response.Data = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = "An error occured!";
                response.Success = false;
                return response;
            }
        }

        public async Task<ServiceResponse<List<ClassResponse>>> GetItemClasses(string email)
        {
            var response = new ServiceResponse<List<ClassResponse>>();

            try
            {
                var isUserExist = await _unitOfWork
                    .GenericRepository<User>()
                    .Get(x =>
                    x.IsActive &&
                    x.Email == email
                    )
                    .AnyAsync();

                if (isUserExist)
                {
                    response.Message = "This user is not exist!";
                    response.Success = false;
                    return response;
                }

                var classes = await _unitOfWork
                    .GenericRepository<Class>()
                    .Get(x =>
                    x.IsActive
                    )
                    .Select(x => new ClassResponse
                    {
                        Id = x.Id,
                        ClassNumber = x.ClassNumber,
                    })
                    .ToListAsync();

                response.Data = classes;
                response.Message = "Classes fetched successfully!";
                response.Success = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = "An error occured!";
                response.Success = false;
                return response;
            }
        }

        
        public async Task<ServiceResponse<RateResponse>> EstimateRate(string email, RateRequest request)
        {
            var response = new ServiceResponse<RateResponse>();

        try
        {
            var user = await _unitOfWork
                .GenericRepository<User>()
                .Get(x =>
                x.IsActive &&
                x.Email == email
                )
                .FirstOrDefaultAsync();

            if (user is null)
            {
                response.Message = "This user is not exist!";
                response.Success = false;
                return response;
            }

            var rate = new Rate
            {
                UserId = user.Id,
                QuoteType = request.QuoteType,
                CODAmount = request.CODAmount,
                DestinationLocation = new Location
                {
                    City = request.Destination.City,
                    StateOrProvince = request.Destination.StateOrProvince,
                    ZipOrPostalCode = request.Destination.ZipOrPostalCode,  
                    CountryCode = request.Destination.CountryCode
                },
                OriginLocation = new Location
                {
                    City = request.Origin.City,
                    StateOrProvince = request.Origin.StateOrProvince,
                    ZipOrPostalCode = request.Origin.ZipOrPostalCode,
                    CountryCode = request.Origin.CountryCode
                }                    
            };

            _unitOfWork.GenericRepository<Rate>().Insert(rate);
            _unitOfWork.Save();

            var responseRate = new RateResponse
            {
                CustomerName = $"{user.FirstName} {user.LastName}",
                Address = user.Address,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                WasSuccess = true,
                Message = "Rate fetched successfully!",
                Destination = new LocationResponse
                {
                    City = rate.DestinationLocation.City,
                    CountryCode = rate.DestinationLocation.CountryCode,
                    StateOrProvince = rate.DestinationLocation.StateOrProvince,
                    ZipOrPostalCode = rate.DestinationLocation.ZipOrPostalCode
                },
                Origin = new LocationResponse
                {
                    City = rate.OriginLocation.City,
                    CountryCode = rate.OriginLocation.CountryCode,
                    StateOrProvince = rate.OriginLocation.StateOrProvince,
                    ZipOrPostalCode = rate.OriginLocation.ZipOrPostalCode
                }
            };

            response.Data = responseRate;
            response.Message = "Rated successfully!";
            response.Success = true;
            return response;
        }
        catch (Exception ex)
        {
            response.Message = "An error occured!";
            response.Success = false;
            return response;
        }
    }
    }
}
