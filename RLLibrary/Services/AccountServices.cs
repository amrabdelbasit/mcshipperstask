﻿using Application.Interfaces;
using Domain.DTO.Account;
using AutoMapper;
using Domain.Models.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IO;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Domain.Models.Account;

namespace Application.Services
{
    public class AccountServices : IAccount
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;

        public AccountServices(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }
       
        public async Task<ServiceResponse<bool>> Register(RegisterRequest request)
        {
            var response = new ServiceResponse<bool>();

            try
            {
                var isUserExisting = await _unitOfWork
                    .GenericRepository<User>()
                    .Get(x =>
                    x.Email == request.Email
                    )
                    .AnyAsync();

                if (isUserExisting)
                {
                    response.Message = "This email is already existing!";
                    response.Success = false;
                    return response;
                }

                CreatePasswordHash(request.Password, out byte[] passwordHash, out byte[] passwordSalt);

                var user = new User
                {
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email,
                    Address = request.Address,
                    PhoneNumber = request.PhoneNumber,
                    PassswordHash = passwordHash,
                    PasswordSalt = passwordSalt,
                    IsWhatsAppNumber = request.IsWhatsAppNumber
                };
             
                _unitOfWork.GenericRepository<User>().Insert(user);
                _unitOfWork.Save();

                response.Data = true;
                response.Success = true;
                response.Message = "Registered successfully!";             
                return response;
            }
            catch (Exception ex)
            {
                response.Message = "An erorr occurred!";
                response.Success = false;
                return response;
            }
        }

        public async Task<ServiceResponse<UserResponseData>> Login(LoginRequest request)
        {
            var response = new ServiceResponse<UserResponseData>();
            try
            {
                var user = await _unitOfWork
                    .GenericRepository<User>()
                    .Get(x =>
                    x.IsActive &&
                    x.Email == request.Email
                    )
                    .FirstOrDefaultAsync();

                if (user is null)
                {
                    response.Message = "This user is not existing!";
                    response.Success = false;
                    return response;
                }

                if (!VerifyPassword(request.Password, user.PassswordHash, user.PasswordSalt))
                {
                    response.Message = "Incorrect password";
                    response.Success = false;
                    return response;
                }
                
                var userResponse = new UserResponseData
                {
                    Id = user.Id,
                    FullName = $"{user.FirstName} {user.LastName}",
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                    Token = CreateToken(user),                  
                };

                _unitOfWork.GenericRepository<User>().Update(user);
                _unitOfWork.Save();

                response.Message = "Logged in successfully!";
                response.Success = true;
                response.Data = userResponse;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = "An error occured!";
                response.Success = false;
                return response;
            }
        }

        public async Task<ServiceResponse<bool>> ResetPassword(string email, string newPassword)
        {
            var response = new ServiceResponse<bool>();

            try
            {
                var user = await _unitOfWork
                    .GenericRepository<User>()
                    .Get(x => 
                    x.IsActive && 
                    x.Email == email
                    )
                    .FirstOrDefaultAsync();

                if (user is null)
                {
                    response.Message = "This user is not exist!";
                    response.Success = false;
                    return response;
                }

                CreatePasswordHash(newPassword, out byte[] passwordHash, out byte[] passwordSalt);
                user.PassswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                _unitOfWork.GenericRepository<User>().Update(user);
                _unitOfWork.Save();

                response.Message = "Reset password successfully!";
                response.Success = true;
                response.Data = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = "An error occured!";
                response.Success = false;
                return response;
            }
        }

        public string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim>{
                new Claim(ClaimTypes.NameIdentifier,user.Email),
                 new Claim(ClaimTypes.Name, user.PhoneNumber)
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value)
                );
            SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(30),
                SigningCredentials = credentials
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }        

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

            }
        }

        private bool VerifyPassword(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {

                var LoginPasswoed = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < LoginPasswoed.Length; i++)
                {
                    if (LoginPasswoed[i] != passwordHash[i])
                    {
                        return false;
                    }
                }
                return true;

            }
        }
    }
}