﻿using Application.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Application.Services
{
	public class GenericRepository<TEntity> : IReprository<TEntity> where TEntity : class

	{
        internal DataContext context;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(DataContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }



        #region GetFunctions
        public async Task<IEnumerable<TEntity>> GetWithCaching(Expression<Func<TEntity, bool>> filter = null,
                                           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                                           int? skip = null, int? take = null,
                                           int ExpirationMins = 5,
                                           params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var data = Get(filter, orderBy, skip, take, includeProperties);
            var results = await data.FromCacheAsync(new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = new TimeSpan(0, ExpirationMins, 0)
            });
            return results;
        }


        public virtual IQueryable<TEntity> Get(
           Expression<Func<TEntity, bool>> filter = null,
           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
           int? skip = null,
           int? take = null,
           params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (orderBy != null)
            {
                query = orderBy(query);
            }
            if (includeProperties != null)
            {
                query = includeProperties.Aggregate(query,
                          (current, include) => current.IncludeOptimized(include));
            }


            if (skip.HasValue && skip > 0)
            {
                query = query.Skip(skip.Value);
            }
            if (take.HasValue && take > 0)
            {
                query = query.Take(take.Value);
            }
            //to avoid duplicated queries aganist database as return type is IQueyrable
            //var result = query.ToList();
            return query;
        }


        //[Obsolete("deprecated",error : false)]
        //public virtual IQueryable<TEntity> Get(
        //  Expression<Func<TEntity, bool>> filter = null,
        //  Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        //  int? skip = null,
        //  int? take = null,
        //  string includeProperties = "")
        //{
        //    IQueryable<TEntity> query = dbSet;

        //    if (filter != null)
        //    {
        //        query = query.Where(filter);
        //    }

        //    foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
        //    {
        //        //query = query.Include(includeProperty);
        //        query = query.IncludeOptimizedByPath(includeProperty);
        //    }

        //    if (orderBy != null)
        //    {
        //        query = orderBy(query);
        //    }
        //    if (skip.HasValue && skip > 0)
        //    {
        //        query = query.Skip(skip.Value);
        //    }
        //    if (take.HasValue && take > 0)
        //    {
        //        query = query.Take(take.Value);
        //    }
        //    //to avoid duplicated queries aganist database as return type is IQueyrable
        //    //var result = query.ToList();
        //    return query;
        //}


        public virtual int GetCount(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            var result = query.Count();
            return result;
        }
        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }

        public TEntity GetSingle(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            TEntity item = null;

            IQueryable<TEntity> dbQuery = dbSet;

            //Apply eager loading
            if (includeProperties != null)
            {
                dbQuery = includeProperties.Aggregate(dbQuery,
                          (current, include) => current.IncludeOptimized(include));
            }

            item = dbQuery.FirstOrDefault(where);

            return item;
        }
        public async Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            TEntity item = null;

            IQueryable<TEntity> dbQuery = dbSet;

            //Apply eager loading
            if (includeProperties != null)
            {
                dbQuery = includeProperties.Aggregate(dbQuery,
                          (current, include) => current.IncludeOptimized(include));
            }

            item = await dbQuery.FirstOrDefaultAsync(where).ConfigureAwait(true);

            return item;
        }
        public IEnumerable<TEntity> GetAllBy(Expression<Func<TEntity, bool>> Predicate)
        {
            return dbSet.Where(Predicate);//.ToList();
        }
        #endregion

        #region DeleteFunctions
        public virtual void Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public void Delete(Expression<Func<TEntity, bool>> Predicate)
        {
            var EntityTobeDeleted = dbSet.Where(Predicate).FirstOrDefault();
            dbSet.Remove(EntityTobeDeleted);
        }

        public void DeleteMany(Expression<Func<TEntity, bool>> Predicate)
        {
            List<TEntity> DeletedEnitities = dbSet.Where(Predicate).ToList();
            foreach (var item in DeletedEnitities)
            {
                dbSet.Remove(item);
            }
        }
        public void DeleteMany(ICollection<TEntity> TEntityList)
        {

            foreach (var item in TEntityList)
            {
                if (context.Entry(item).State == EntityState.Deleted)
                {
                    dbSet.Attach(item);
                }
                dbSet.Remove(item);
            }
        }
        #endregion

        #region OtherFunctions

        public virtual void Insert(TEntity entity)
        {
            dbSet.Add(entity);
        }
        public virtual void InsertAll(List<TEntity> entities)
        {
            dbSet.AddRange(entities);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }
        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }
        public bool IsExist(Expression<Func<TEntity, bool>> Predicate)
        {

            return dbSet.Where(Predicate).Any();
        }


        public void BulkUpdate(List<TEntity> Entities, bool UpdateIncludeChildren = false)
        {
            dbSet.BulkUpdate(Entities, options => options.IncludeGraph = UpdateIncludeChildren);
        }

        public int BatchUpdate(Expression<Func<TEntity, bool>> where, Expression<Func<TEntity, TEntity>> updated)
        {
            return dbSet.Where(where).Update(updated);
        }
        public void BulkInsert(List<TEntity> Entities)
        {
            dbSet.BulkInsert(Entities);
        }

        public void BulkDelete(List<TEntity> Entities)
        {
            dbSet.BulkDelete(Entities);
        }

        public int BatchDelete(Expression<Func<TEntity, bool>> where)
        {
            return dbSet.Where(where).Delete();
        }

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~GenericRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        #endregion

    }
}
