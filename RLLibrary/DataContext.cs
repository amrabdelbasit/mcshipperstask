﻿using Domain.Models.Account;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence
{
	public class DataContext: DbContext
	{
		public DataContext(DbContextOptions<DataContext> options) : base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//modelBuilder.Entity<CountryLocalization>().HasKey(cl => new { cl.LanguageId, cl.CountryId });			


			//modelBuilder.Entity<User>()
			//    .HasMany(u => u.UserRates)
			//    .WithOne(ur => ur.User)
			//    .HasForeignKey(ur => ur.UserId)
			//    .OnDelete(DeleteBehavior.NoAction);

		}

		public DbSet<User> Users { get; set; }

	}
}
