﻿using Domain.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DTO.Account
{
	public class UserResponseData
	{
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string Token { get; set; }                     
      
    }  
}
