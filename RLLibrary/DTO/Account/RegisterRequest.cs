﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Domain.DTO.Account
{
   public class RegisterRequest
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; } 
        
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password),
            RegularExpression(@"^(01)(0|1|2|5)\d{8}$")]
        public string PhoneNumber { get; set; }

        [Required]
        public bool IsWhatsAppNumber { get; set; }

        [Required]
        public string Address { get; set; }
    }
}
