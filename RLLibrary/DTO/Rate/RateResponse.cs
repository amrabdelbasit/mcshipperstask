﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLLibrary.DTO.Rate
{
    public class RateResponse
    {
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public bool WasSuccess { get; set; }
        public string Message { get; set; }
        public LocationResponse Origin { get; set; }
        public LocationResponse Destination { get; set; }
    }

    public class LocationResponse
    {
        public string City { get; set; }
        public string StateOrProvince { get; set; }
        public string ZipOrPostalCode { get; set; }
        public string CountryCode { get; set; }

    }
}
