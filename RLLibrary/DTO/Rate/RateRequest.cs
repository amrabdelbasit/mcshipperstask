﻿using RLLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLLibrary.DTO.Rate
{
    public class RateRequest
    {
        public QuoteType QuoteType { get; set; }
        public decimal CODAmount { get; set; }

        [Required]
        public LocationRequest Origin { get; set; }

        [Required]
        public LocationRequest Destination { get; set; }

        [Required, MinLength(1), MaxLength(8)]
        public ICollection<ItemRequest> Items { get; set; }



    }

    public class LocationRequest
    {
        public string City { get; set; }
        public string StateOrProvince { get; set; }

        [Required]
        public string ZipOrPostalCode { get; set; }

        [Required]
        public string CountryCode { get; set; }

    }

    public class ItemRequest
    {
        [Required]
        public Guid ClassId { get; set; }

        [Required]
        public float Weight { get; set; }

        public float Height { get; set; }
        public float Width { get; set; }
        public float Length { get; set; }
    }
}
