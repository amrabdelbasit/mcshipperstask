﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLLibrary.DTO.Rate
{
    public class ClassResponse
    {
        public Guid Id { get; set; }
        public decimal ClassNumber { get; set; }
    }
}
