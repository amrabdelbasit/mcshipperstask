﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Shared
{
    public enum ActionStatus
    {
        Add = 1,
        Update = 2,
        Delete = 3
    }    
}
