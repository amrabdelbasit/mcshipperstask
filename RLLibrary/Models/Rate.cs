﻿using Domain.Models.Account;
using Domain.Models.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLLibrary.Models
{
    [Table(name: "Rate")]
    public class Rate : BaseEntity
    {
        public QuoteType QuoteType { get; set; }
        public decimal CODAmount { get; set; }

        public virtual ICollection<Item> Items { get; set; }

        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public User User { get; set; }

        [ForeignKey("OriginLocation")]
        public Guid OriginLocationId { get; set; }
        public Location OriginLocation { get; set; }

        [ForeignKey("DestinationLocation")]
        public Guid DestinationLocationId { get; set; }
        public Location DestinationLocation { get; set; }
    }

    public enum QuoteType
    {
        Domestic = 0,
        AlaskaHawaii = 1,
        International = 2
    }
}
