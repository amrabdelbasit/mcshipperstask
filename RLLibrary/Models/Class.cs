﻿using Domain.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLLibrary.Models
{
    public class Class : BaseEntity 
    {
        public decimal ClassNumber { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}
