﻿using Domain.Models.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLLibrary.Models
{
    public class Location : BaseEntity
    {
        public string City { get; set; }
        public string StateOrProvince { get; set; }
        public string ZipOrPostalCode { get; set; }
        public string CountryCode { get; set; }

        [InverseProperty("OriginLocation")]
        public virtual ICollection<Rate> OriginRates { get; set; }

        [InverseProperty("DestinationLocation")]
        public virtual ICollection<Rate> DestinationRates { get; set; }



    }
}
