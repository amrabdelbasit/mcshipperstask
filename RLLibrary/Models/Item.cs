﻿using Domain.Models.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLLibrary.Models
{
    [Table(name: "Item")]
    public class Item : BaseEntity
    {
        public float Weight { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public float Length { get; set; }

        [ForeignKey("Class")]
        public Guid ClassId { get; set; }
        public Class Class { get; set; }

        [ForeignKey("Rate")]
        public Guid RateId { get; set; }
        public Rate Rate { get; set; }
    }  
}
