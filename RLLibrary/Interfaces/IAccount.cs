﻿using Domain.DTO.Account;
using Domain.Models.Account;
using Domain.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
	public interface IAccount
	{		
		Task<ServiceResponse<bool>> Register(RegisterRequest request);		
		Task<ServiceResponse<UserResponseData>> Login(LoginRequest request);
		Task<ServiceResponse<bool>> ResetPassword(string email, string password);
	}
}
