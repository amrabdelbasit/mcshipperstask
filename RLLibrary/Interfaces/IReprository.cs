﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
	public interface IReprository<TEntity> : IDisposable where TEntity : class
    {
        void Delete(object id);
        void Delete(TEntity entityToDelete);
        void Delete(Expression<Func<TEntity, bool>> Predicate);
        void DeleteMany(Expression<Func<TEntity, bool>> Predicate);
        // IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, int? skip = null, int? take = null, string includeProperties = "");
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, int? skip = null, int? take = null, params Expression<Func<TEntity, object>>[] includeProperties);
        Task<IEnumerable<TEntity>> GetWithCaching(Expression<Func<TEntity, bool>> filter = null,
                                           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                                           int? skip = null, int? take = null,
                                           int ExpirationMins = 5,
                                           params Expression<Func<TEntity, object>>[] includeProperties);

        int GetCount(Expression<Func<TEntity, bool>> filter = null);

        TEntity GetByID(object id);
        TEntity GetSingle(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includeProperties);
        void Insert(TEntity entity);
        void InsertAll(List<TEntity> entity);
        void Update(TEntity entityToUpdate);
        bool IsExist(Expression<Func<TEntity, bool>> Predicate);
        IEnumerable<TEntity> GetAllBy(Expression<Func<TEntity, bool>> Predicate);

        /// <summary>
        ///  no need to call save change as this function work out of scope of unitOfWork
        /// </summary>
        /// <param name="Entities"></param>
        /// <param name="UpdateIncludeChildren"></param>
        void BulkUpdate(List<TEntity> Entities, bool UpdateIncludeChildren = false);


        int BatchUpdate(Expression<Func<TEntity, bool>> where, Expression<Func<TEntity, TEntity>> updated);
        /// <summary>
        ///  no need to call save change as this function work out of scope of unitOfWork
        /// </summary>
        /// <param name="Entities"></param>
        void BulkInsert(List<TEntity> Entities);

        void BulkDelete(List<TEntity> Entities);
        int BatchDelete(Expression<Func<TEntity, bool>> where);

    }
}
