﻿using Domain.Models.Shared;
using RLLibrary.DTO.Rate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLLibrary.Interfaces
{
    public interface IRate
    {
        Task<ServiceResponse<bool>> AddItemClasses(string email, List<decimal> classesNumbers);

        Task<ServiceResponse<List<ClassResponse>>> GetItemClasses(string email);

        Task<ServiceResponse<RateResponse>> EstimateRate(string email, RateRequest request);
    }
}
