﻿using Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
	public interface IUnitOfWork : IDisposable
	{

		GenericRepository<T> GenericRepository<T>() where T : class, new();

		int Save();
	}
}
